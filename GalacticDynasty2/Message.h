#pragma once
#include <string>
#include <vector>
class Message
{
public:
	Message(int i, std::string f, std::string m, bool s, time_t ts) {
		id = i;
		from = f;
		msg = m;
		seen = s;
		timestamp = ts;
	}
	int id;
	std::string from;
	std::string msg;
	bool seen;
	time_t timestamp;
};

