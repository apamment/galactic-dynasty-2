#include <iostream>
#include <cstring>
#include <algorithm>
#include <cstdint>
#include <cmath>
#ifdef _MSC_VER
#define strcasecmp stricmp
#endif
extern "C" {
#include "magidoor/MagiDoor.h"
}
#include "Empire.h"
#include "Message.h"
#include "sqlite3.h"

extern uint32_t turns_per_day;
extern uint32_t turns_in_protection;

static inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
		return !std::isspace(ch);
		}));
}

// trim from end (in place)
static inline void rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
		return !std::isspace(ch);
		}).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string& s) {
	ltrim(s);
	rtrim(s);
}

uint64_t gd_get_uint64(uint64_t quickmax) {
	char c;
	uint64_t retval = 0;

	static char lastc = 'x';


	while (1) {
		c = md_getc();
		if (c == '\n' || c == '\0') {
			lastc = c;
			if (lastc == '\r') {
				continue;
			}
			else {
				return retval;
			}
		}
		if (c == '\r') {
			lastc = c;
			return retval;
		}
		if (c == '\b' || c == 127) {
			if (retval > 0) {
				retval /= 10;
				md_printf("\b \b");

			}
			lastc = c;
			continue;
		}
		if (c == '>' && retval == 0) {
			if (quickmax > 0) {
				md_printf("%" PRIu64, quickmax);
				lastc = c;
				retval = quickmax;
			}
		}
		if (c >= '0' && c <= '9') {
			retval = retval * 10 + (uint64_t)((uint64_t)c - '0');
			md_putchar(c);
		}
		lastc = c;
	}
}

int Empire::list_empires(int64_t myid) {
	sqlite3* db;
	sqlite3_stmt* stmt;
	static const char* sql = "SELECT empname FROM empires WHERE id != ?";

	if (!open_database(&db)) {
		return 0;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return 0;
	}
	sqlite3_bind_int64(stmt, 1, myid);

	int ret = 0;
	md_printf("\r\n");
	while (sqlite3_step(stmt) == SQLITE_ROW) {
		md_printf("`bright white`%s`white`\r\n", (const char*)sqlite3_column_text(stmt, 0));
		ret++;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);
	return ret;
}

int Empire::new_messages()
{
	static const char* sql = "SELECT COUNT(*) from messages where msgto = ? and msgseen = 0";
	sqlite3* db;
	sqlite3_stmt* stmt;

	if (!open_database(&db)) {
		return 0;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return 0;
	}
	sqlite3_bind_int64(stmt, 1, id);

	int ret = 0;

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		ret = sqlite3_column_int(stmt, 0);
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return ret;
}

uint64_t Empire::calculate_score() {
	uint64_t score;
	uint64_t planet_count = ((uint64_t)planets_ore + (uint64_t)planets_food + (uint64_t)planets_industrial + (uint64_t)planets_military + (uint64_t)planets_tech);

	score = 0;
	score += (uint64_t)troops / 5;
	score += (uint64_t)generals;
	score += (uint64_t)fighters * 2;
	score += (uint64_t)defence_stations * 2;
	score += (uint64_t)command_ship * 60;

	if (planet_count <= 20) {
		score += planet_count * 20;
	}
	else {
		score += 400 + planet_count;
	}
	score += (uint64_t)food / 10000;
	score += (uint64_t)population / 1000;
	score += ((uint64_t)sprockets / 10000);
	score += ((uint64_t)ore / 10000);
	score /= 100;

	return score;
}

void Empire::set_research_level(std::string name, int level) {
	sqlite3 *db;
	sqlite3_stmt *stmt;
	static const char *sql = "SELECT level FROM research WHERE research = ? and eid = ?";
	static const char *sql2 = "UPDATE research SET level = ? WHERE research = ? and eid = ?";
	static const char *sql3 = "INSERT INTO research (eid, research, level) VALUES(?, ?, ?)";

	if (!open_database(&db)) {
		md_printf("\r\n\r\n`bright red`Failed to open database!`white`\r\n\r\n");
		return;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}
	sqlite3_bind_text(stmt, 1, name.c_str(), -1, NULL);
	sqlite3_bind_int64(stmt, 2, id);

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		sqlite3_finalize(stmt);
		if (sqlite3_prepare_v2(db, sql2, -1, &stmt, NULL) != SQLITE_OK) {
			sqlite3_close(db);
			return;
		}
		sqlite3_bind_int(stmt, 1, level);
		sqlite3_bind_text(stmt, 2, name.c_str(), -1, NULL);
		sqlite3_bind_int64(stmt, 3, id);

		sqlite3_step(stmt);
		sqlite3_finalize(stmt);
		sqlite3_close(db);

		return;
	}
	sqlite3_finalize(stmt);
	if (sqlite3_prepare_v2(db, sql3, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}
	sqlite3_bind_int64(stmt, 1, id);
	sqlite3_bind_text(stmt, 2, name.c_str(), -1, NULL);
	sqlite3_bind_int(stmt, 3, level);
	
	sqlite3_step(stmt);
	sqlite3_finalize(stmt);
	sqlite3_close(db);
}

int Empire::get_research_level(std::string name) {
	sqlite3 *db;
	sqlite3_stmt *stmt;
	static const char *sql = "SELECT level FROM research WHERE research = ? and eid = ?";

	if (!open_database(&db)) {
		md_printf("\r\n\r\n`bright red`Failed to open database!`white`\r\n\r\n");
		return 0;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return 0;
	}
	sqlite3_bind_text(stmt, 1, name.c_str(), -1, NULL);
	sqlite3_bind_int64(stmt, 2, id);

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		int ret = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		return ret;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return 0;
}

bool Empire::open_database(sqlite3** db) {
	static const char* create_users_sql = "CREATE TABLE IF NOT EXISTS empires(id INTEGER PRIMARY KEY, "
		"bbsname TEXT, "
		"empname TEXT COLLATE NOCASE,"
		"troops INTEGER,"
		"generals INTEGER,"
		"fighters INTEGER,"
		"defencestations INTEGER,"
		"spies INTEGER,"
		"population INTEGER,"
		"food INTEGER,"
		"credits INTEGER,"
		"bankbalance INTEGER,"
		"sprockets INTEGER,"
		"ore INTEGER,"
		"tech_points INTEGER,"
		"planets_food INTEGER,"
		"planets_ore INTEGER,"
		"planets_industrial INTEGER,"
		"planets_military INTEGER,"
		"planets_urban INTEGER,"
		"planets_tech INTEGER,"
		"commandship INTEGER,"
		"lastplayed INTEGER,"
		"turnsleft INTEGER,"
		"totalturns INTEGER,"
		"researching TEXT,"
		"winner INTEGER);";
	static const char* create_msgs_sql = "CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY, "
		"msgto INTEGER,"
		"msgfrom TEXT COLLATE NOCASE,"
		"msgbody TEXT,"
		"msgseen INTEGER,"
		"msgtime INTEGER);";
	static const char* create_research_sql = "CREATE TABLE IF NOT EXISTS research(eid INTEGER, research TEXT, level INTEGER);";

	int rc;
	char* err_msg = NULL;

	if (sqlite3_open("empires.sqlite3", db) != SQLITE_OK) {
		std::cerr << "Unable to open database!" << std::endl;
		return false;
	}
	sqlite3_busy_timeout(*db, 5000);

	rc = sqlite3_exec(*db, create_users_sql, 0, 0, &err_msg);
	if (rc != SQLITE_OK) {
		std::cerr << "Unable to create empire table: " << err_msg << std::endl;
		free(err_msg);
		sqlite3_close(*db);
		return false;
	}
	rc = sqlite3_exec(*db, create_msgs_sql, 0, 0, &err_msg);
	if (rc != SQLITE_OK) {
		std::cerr << "Unable to create messages table: " << err_msg << std::endl;
		free(err_msg);
		sqlite3_close(*db);
		return false;
	}
	rc = sqlite3_exec(*db, create_research_sql, 0, 0, &err_msg);
	if (rc != SQLITE_OK) {
		std::cerr << "Unable to create research table: " << err_msg << std::endl;
		free(err_msg);
		sqlite3_close(*db);
		return false;
	}

	return true;
}

bool Empire::new_player(std::string bbsname) {
	char empirename[16];

	this->bbsname = bbsname;

	md_clr_scr();
	md_sendfile("ansi/instruction.ans", 1);

	md_printf("\r\n`white`Welcome to `bright blue`Galactic Dynasty`white`!\r\n");
	md_printf("What would you like to name your empire: ");

	if (md_getstring(empirename, 16, 32, 126) > 0) {
		name = std::string(empirename);
		trim(name);


		if (strcasecmp(name.c_str(), "system") == 0 || name == "?") {
			md_printf("\r\n`bright red`Sorry, that name is unavailable!`white`\r\n");
			return false;
		}
		if (name.size() > 0) {
			// check if it exists.
			sqlite3* db;
			sqlite3_stmt* stmt;
			static const char* sqlcheck = "SELECT id FROM empires WHERE empname = ?";
			static const char* insertsql = "INSERT INTO empires (bbsname, empname, troops, generals, fighters, defencestations, spies, population, food, credits, bankbalance, sprockets, ore, tech_points, planets_food, planets_ore,"
				"planets_industrial, planets_military, planets_urban, planets_tech, commandship, lastplayed, turnsleft, totalturns, researching, winner) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0)";
			if (!open_database(&db)) {
				return false;
			}

			if (sqlite3_prepare_v2(db, sqlcheck, -1, &stmt, NULL) != SQLITE_OK) {
				sqlite3_close(db);
				return false;
			}

			sqlite3_bind_text(stmt, 1, name.c_str(), -1, NULL);

			if (sqlite3_step(stmt) == SQLITE_ROW) {
				sqlite3_finalize(stmt);
				sqlite3_close(db);

				md_printf("\r\n`bright red`Sorry, that empire exists already!`white`\r\n");
				return false;
			}
			sqlite3_finalize(stmt);
			md_printf("\r\n\r\nDoes the mighty empire of `bright green`%s`white` sound ok? (Y/N) ", name.c_str());
			char c = md_get_answer((char *)"yYnN");
			if (tolower(c) == 'n')
			{
				sqlite3_close(db);
				return false;
			}


			troops = 50;
			generals = 0;
			fighters = 0;
			defence_stations = 0;
			spies = 0;

			population = 240;
			food = 50;
			credits = 2000;

			planets_food = 4;
			planets_ore = 1;
			planets_industrial = 0;
			planets_military = 0;
			planets_urban = 6;
			planets_tech = 0;
			command_ship = 0;

			turns_left = turns_per_day;
			last_played = time(NULL);
			total_turns = 0;
			bank_balance = 0;
			sprockets = 0;
			ore = 0;
			tech_points = 0;
			researching = "Agriculture";

			if (sqlite3_prepare_v2(db, insertsql, -1, &stmt, NULL) != SQLITE_OK) {
				md_printf("%s\n", sqlite3_errmsg(db));
				sqlite3_close(db);
				return false;
			}

			sqlite3_bind_text(stmt, 1, bbsname.c_str(), -1, NULL);
			sqlite3_bind_text(stmt, 2, name.c_str(), -1, NULL);
			sqlite3_bind_int(stmt, 3, troops);
			sqlite3_bind_int(stmt, 4, generals);
			sqlite3_bind_int(stmt, 5, fighters);
			sqlite3_bind_int(stmt, 6, defence_stations);
			sqlite3_bind_int(stmt, 7, spies);
			sqlite3_bind_int(stmt, 8, population);
			sqlite3_bind_int(stmt, 9, food);
			sqlite3_bind_int64(stmt, 10, credits);
			sqlite3_bind_int64(stmt, 11, bank_balance);
			sqlite3_bind_int(stmt, 12, sprockets);
			sqlite3_bind_int(stmt, 13, ore);
			sqlite3_bind_int(stmt, 14, tech_points);
			sqlite3_bind_int(stmt, 15, planets_food);
			sqlite3_bind_int(stmt, 16, planets_ore);
			sqlite3_bind_int(stmt, 17, planets_industrial);
			sqlite3_bind_int(stmt, 18, planets_military);
			sqlite3_bind_int(stmt, 19, planets_urban);
			sqlite3_bind_int(stmt, 20, planets_tech);
			sqlite3_bind_int(stmt, 21, command_ship);
			sqlite3_bind_int64(stmt, 22, last_played);
			sqlite3_bind_int(stmt, 23, turns_left);
			sqlite3_bind_int(stmt, 24, total_turns);
			sqlite3_bind_text(stmt, 25, researching.c_str(), -1, NULL);

			if (sqlite3_step(stmt) != SQLITE_DONE) {
				md_printf("%s\n", sqlite3_errmsg(db));
				sqlite3_finalize(stmt);
				sqlite3_close(db);
				return false;
			}
			id = sqlite3_last_insert_rowid(db);
			sqlite3_finalize(stmt);
			sqlite3_close(db);

			return true;
		}
	}
	return false;
}
void Empire::send_message(int64_t toid, std::string from, std::string msg) {
	static const char* msgsql = "INSERT INTO messages (msgto, msgfrom, msgbody, msgseen, msgtime) VALUES(?, ?, ?, 0, ?)";
	sqlite3* db;
	sqlite3_stmt* stmt;

	if (!open_database(&db)) {
		return;
	}

	if (sqlite3_prepare_v2(db, msgsql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}
	time_t now = time(NULL);
	sqlite3_bind_int64(stmt, 1, toid);
	sqlite3_bind_text(stmt, 2, from.c_str(), -1, NULL);
	sqlite3_bind_text(stmt, 3, msg.c_str(), -1, NULL);
	sqlite3_bind_int64(stmt, 4, now);

	sqlite3_step(stmt);
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return;
}

void Empire::do_battle(int64_t mid, uint32_t* tattack, uint32_t* gattack, uint32_t* fattack)
{
	Empire otheremp;

	if (otheremp.load(mid) != 1) {
		md_printf("\r\n\r\n`bright red`Failed to find the empire!`white`\r\n");
		return;
	}
	if (otheremp.total_turns <= turns_in_protection) {
		md_printf("\r\n\r\n`bright red`Empire is in protection!`white`\r\n");
		return;
	}
	uint32_t attack = (3 * (*tattack) + 1 * (*fattack)) + ((3 * (*tattack) + 1 * (*fattack)) * ((*gattack) / 100));
	uint32_t defence = 10 * otheremp.troops;

	// attack defence stations
	attack = attack + (1 * (*tattack) + 4 * (*fattack) + ((1 * (*tattack) + 4 * (*fattack)) * ((*gattack) / 100)));
	defence = defence + (25 * otheremp.defence_stations);

	long double victory_chance = ((long double)attack / ((long double)attack + (long double)defence));
	int battle = rand() % 100 + 1;

	if (battle < (victory_chance * 100)) {
		md_printf("\r\n\r\n`white`You are `bright green`victorious`white`, you plundered:\r\n");
		// people
		uint32_t plunder_people = (uint32_t)((double)otheremp.population * 0.10);
		if (plunder_people > 0)
		{
			otheremp.population -= plunder_people;
		}

		// credits
		uint64_t plunder_credits = (uint64_t)((long double)otheremp.credits * 0.10);
		if (plunder_credits > 0)
		{
			otheremp.credits -= plunder_credits;
			credits += plunder_credits;
			md_printf("   - %" PRIu64 " credits\r\n", plunder_credits);
		}
		// food
		uint32_t plunder_food = (uint32_t)((double)otheremp.food * 0.10);
		if (plunder_food > 0)
		{
			otheremp.food -= plunder_food;
			food += plunder_food;
			md_printf("   - %u food\r\n", plunder_food);
		}
		// planets
		uint32_t plunder_planet_ore = (uint32_t)((double)otheremp.planets_ore * 0.05);
		if (plunder_planet_ore > 0)
		{
			otheremp.planets_ore -= plunder_planet_ore;
			planets_ore += plunder_planet_ore;
			md_printf("   - %u ore planets\r\n", plunder_planet_ore);
		}
		uint32_t plunder_planet_food = (uint32_t)((double)otheremp.planets_food * 0.05);
		if (plunder_planet_food > 0)
		{
			otheremp.planets_food -= plunder_planet_food;
			planets_food += plunder_planet_food;
			md_printf("   - %u food planets\r\n", plunder_planet_food);
		}
		uint32_t plunder_planet_industrial = (uint32_t)((double)otheremp.planets_industrial * 0.05);
		if (plunder_planet_industrial > 0)
		{
			otheremp.planets_industrial -= plunder_planet_industrial;
			planets_industrial += plunder_planet_industrial;
			md_printf("   - %u industrial planets\r\n", plunder_planet_industrial);
		}
		uint32_t plunder_planet_military = (uint32_t)((double)otheremp.planets_military * 0.05);
		if (plunder_planet_military > 0)
		{
			otheremp.planets_military -= plunder_planet_military;
			planets_military += plunder_planet_military;
			md_printf("   - %u soldier planets\r\n", plunder_planet_military);
		}

		int difference = rand() % 5 + 1;

		int dtroops = (int)((uint32_t)(*tattack) - (uint32_t)((double)(*tattack) * ((double)difference / 100.f)));
		int dgenerals = (int)((uint32_t)(*gattack) - (uint32_t)((double)(*gattack) * ((double)difference / 100.f)));
		int dfighters = (int)((uint32_t)(*fattack) - (uint32_t)((double)(*fattack) * ((double)difference / 100.f)));

		if ((uint32_t)dtroops > otheremp.troops)
			dtroops = otheremp.troops;
		if ((uint32_t)dgenerals > otheremp.generals)
			dgenerals = otheremp.generals;
		if ((uint32_t)dfighters > otheremp.defence_stations)
			dfighters = otheremp.defence_stations;

		uint32_t enemy_troops = dtroops;
		uint32_t enemy_generals = dgenerals;
		uint32_t enemy_defence_stations = dfighters;

		dtroops = (int)((uint32_t)(*tattack) - ((uint32_t)otheremp.troops - (uint32_t)((double)otheremp.troops * ((double)difference / 100.f))));
		dgenerals = (int)((uint32_t)(*gattack) - ((uint32_t)otheremp.generals - (uint32_t)((double)otheremp.generals * ((double)difference / 100.f))));
		dfighters = (int)((uint32_t)(*fattack) - ((uint32_t)otheremp.defence_stations - (uint32_t)((double)otheremp.defence_stations * ((double)difference / 100.f))));

		if (dtroops < 0)
			dtroops = 0;
		if (dgenerals < 0)
			dgenerals = 0;
		if (dfighters < 0)
			dfighters = 0;

		md_printf("`white`You lost:\r\n");
		md_printf("   - %u Troops\r\n", (*tattack) - dtroops );
		md_printf("   - %u Generals\r\n", (*gattack) - dgenerals);
		md_printf("   - %u Fighters\r\n", (*fattack) - dfighters);

		*tattack = dtroops;
		*gattack = dgenerals;
		*fattack = dfighters;

		otheremp.troops -= enemy_troops;
		otheremp.generals -= enemy_generals;
		otheremp.defence_stations -= enemy_defence_stations;

		otheremp.save();
	
		send_message(mid, "SYSTEM", "You were attacked by " + name + " and lost the battle.");
	}
	else {
		// defeat
		int difference = rand() % 5 + 1;

		int dtroops = (int)((uint32_t)(*tattack) - (uint32_t)((double)(*tattack) * ((double)difference / 100.f)));
		int dgenerals = (int)((uint32_t)(*gattack) - (uint32_t)((double)(*gattack) * ((double)difference / 100.f)));
		int dfighters = (int)((uint32_t)(*fattack) - (uint32_t)((double)(*fattack) * ((double)difference / 100.f)));

		if (dtroops > otheremp.troops)
			dtroops = otheremp.troops;
		if (dgenerals > otheremp.generals)
			dgenerals = otheremp.generals;
		if (dfighters > otheremp.defence_stations)
			dfighters = otheremp.defence_stations;

		uint32_t enemy_troops = dtroops;
		uint32_t enemy_generals = dgenerals;
		uint32_t enemy_defence_stations = dfighters;

		dtroops = (int)((uint32_t)(*tattack) - ((uint32_t)otheremp.troops - (uint32_t)((double)otheremp.troops * ((double)difference / 100.f))));
		dgenerals = (int)((uint32_t)(*gattack) - ((uint32_t)otheremp.generals - (uint32_t)((double)otheremp.generals * ((double)difference / 100.f))));
		dfighters = (int)((uint32_t)(*fattack) - ((uint32_t)otheremp.defence_stations - (uint32_t)((double)otheremp.defence_stations * ((double)difference / 100.f))));

		if (dtroops < 0)
			dtroops = 0;
		if (dgenerals < 0)
			dgenerals = 0;
		if (dfighters < 0)
			dfighters = 0;

		*tattack = dtroops;
		*gattack = dgenerals;
		*fattack = dfighters;

		otheremp.troops -= enemy_troops;
		otheremp.generals -= enemy_generals;
		otheremp.defence_stations -= enemy_defence_stations;

		otheremp.save();

		md_printf("\r\n\r\n`white`You are `bright red`defeated`white`.\r\n");
		send_message(mid, "SYSTEM", "You were attacked by " + name + " and won the battle.");
	}
}

void Empire::show_messages() {
	std::vector<Message> msgs;
	sqlite3* db;
	sqlite3_stmt* stmt;
	static const char* sql = "SELECT id, msgfrom, msgbody, msgseen, msgtime FROM messages WHERE msgto = ?";

	if (!open_database(&db)) {
		return;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}

	sqlite3_bind_int64(stmt, 1, id);

	while (sqlite3_step(stmt) == SQLITE_ROW) {
		Message m(sqlite3_column_int(stmt, 0), std::string((const char*)sqlite3_column_text(stmt, 1)), std::string((const char*)sqlite3_column_text(stmt, 2)), sqlite3_column_int(stmt, 3) == 1, sqlite3_column_int64(stmt, 4));
		msgs.push_back(m);
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);
	bool done = false;

	while (!done) {
		uint32_t msgsnew = 0;
		for (size_t z = 0; z < msgs.size(); z++) {
			if (msgs.at(z).seen == false) {
				msgsnew++;
			}
		}
		md_clr_scr();
		md_printf("`bright magenta`Message Centre\r\n");
		md_printf("`magenta`============================================================\r\n\r\n");
		md_printf("`bright magenta`You have `bright white`%u`bright magenta` messages, `bright white`%u`bright magenta` are new.\r\n\r\n", msgs.size(), msgsnew);
		md_printf("`bright magenta`1. `white`Read new messages\r\n");
		md_printf("`bright magenta`2. `white`Read ALL messages\r\n");
		md_printf("`bright magenta`3. `white`Send a message\r\n");
		md_printf("`bright magenta`Q. `white`Quit message centre\r\n");

		char c = md_get_answer((char*)"123Qq");

		switch (c) {
		case '1':

			if (msgs.size() == 0) {
				md_printf("\r\n`bright red`You have no messages!`white`\r\n\r\n");
			}
			else {
				int linecount = 0;
				for (size_t i = 0; i < msgs.size(); i++) {
					if (msgs.at(i).seen == false) {
						struct tm msgtime;
						linecount += 3;
						if (linecount >= 24) {
							md_printf("`white`Press a key to continue...");
							md_getc();
							md_printf("\r\n");
							linecount = 3;
						}
#ifdef _MSC_VER
						localtime_s(&msgtime, &msgs.at(i).timestamp);
#else
						localtime_r(&msgs.at(i).timestamp, &msgtime);
#endif
						md_printf("`bright yellow`Message from `bright white`%s`bright yellow` dated `bright white`%04d-%02d-%02d`bright yellow`:\r\n", msgs.at(i).from.c_str(), msgtime.tm_year + 1900, msgtime.tm_mon + 1, msgtime.tm_mday);
						md_printf("`white`%s\r\n\r\n", msgs.at(i).msg.c_str());
						msgs.at(i).seen = true;
					}
				}
				if (linecount == 0) {
					md_printf("\r\n`bright red`You have no unread messages!`white`\r\n\r\n");
				}
				else {
					if (open_database(&db)) {
						static const char* updatesql = "UPDATE messages SET msgseen = 1 WHERE msgto = ?";
						if (sqlite3_prepare_v2(db, updatesql, -1, &stmt, NULL) == SQLITE_OK) {
							sqlite3_bind_int64(stmt, 1, id);
							sqlite3_step(stmt);
							sqlite3_finalize(stmt);
						}
						sqlite3_close(db);
					}
				}
			}
			md_printf("`white`Press a key to continue...");
			md_getc();
			break;
		case '2':
			if (msgs.size() == 0) {
				md_printf("\r\n`bright red`You have no messages!`white`\r\n\r\n");
			}
			else {
				int linecount = 0;
				for (size_t i = 0; i < msgs.size(); i++) {
					struct tm msgtime;
					linecount += 3;
					if (linecount >= 24) {
						md_printf("`white`Press a key to continue...");
						md_getc();
						md_printf("\r\n");
						linecount = 3;
					}
#ifdef _MSC_VER
					localtime_s(&msgtime, &msgs.at(i).timestamp);
#else
					localtime_r(&msgs.at(i).timestamp, &msgtime);
#endif
					if (msgs.at(i).seen) {
						md_printf("`bright black`Message from `bright black`%s`bright black` dated `bright black`%04d-%02d-%02d`bright black`:\r\n", msgs.at(i).from.c_str(), msgtime.tm_year + 1900, msgtime.tm_mon + 1, msgtime.tm_mday);
						md_printf("`bright black`%s\r\n\r\n", msgs.at(i).msg.c_str());

					}
					else {
						md_printf("`bright yellow`Message from `bright white`%s`bright yellow` dated `bright white`%04d-%02d-%02d`bright yellow`:\r\n", msgs.at(i).from.c_str(), msgtime.tm_year + 1900, msgtime.tm_mon + 1, msgtime.tm_mday);
						md_printf("`white`%s\r\n\r\n", msgs.at(i).msg.c_str());
						msgs.at(i).seen = true;
					}
				}
				if (open_database(&db)) {
					static const char* updatesql = "UPDATE messages SET msgseen = 1 WHERE msgto = ?";
					if (sqlite3_prepare_v2(db, updatesql, -1, &stmt, NULL) == SQLITE_OK) {
						sqlite3_bind_int64(stmt, 1, id);
						sqlite3_step(stmt);
						sqlite3_finalize(stmt);
					}
					sqlite3_close(db);
				}
			}
			md_printf("`white`Press a key to continue...");
			md_getc();
			break;
		case '3':
			while (true) {
				char buffer[16];
				md_printf("`bright magenta`What empire do you want to send a message to (? to list): `white`");
				if (md_getstring(buffer, 16, 32, 126) > 0) {
					if (buffer[0] == '?' && strlen(buffer) == 1) {
						if (list_empires(id) == 0) {
							md_printf("\r\n`bright red`No other empires exist!`white`\r\n");
							break;
						}
					}
					else {
						sqlite3* db;
						sqlite3_stmt* stmt;
						static const char* sql = "SELECT id FROM empires WHERE empname = ?";

						if (!open_database(&db)) {
							break;
						}
						if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
							sqlite3_close(db);
							break;
						}
						sqlite3_bind_text(stmt, 1, buffer, -1, NULL);
						if (sqlite3_step(stmt) == SQLITE_ROW) {
							int mid = sqlite3_column_int(stmt, 0);
							sqlite3_finalize(stmt);
							sqlite3_close(db);
							md_printf("\r\n\r\n`bright magenta`Please enter your (short) message...`white`\r\n");
							char msgbuf[79];
							if (md_getstring(msgbuf, 79, 32, 126) == 0) {
								
								md_printf("\r\n`bright red`Aborted!`white`\r\n");
								break;
							}
							send_message(mid, name, std::string(msgbuf));
							md_printf("\r\n\r\n`bright green`Message sent!`white`");
							break;
						}
						else {
							sqlite3_finalize(stmt);
							sqlite3_close(db);
							md_printf("\r\n`bright red`No other empires by that name!`white`\r\n");
							break;
						}
					}
				}
				else {
					break;
				}
			}

			md_printf("\r\n\r\n`bright white`Press any key to continue...`white`");
			md_getc();

			break;
		default:
			done = true;
			break;
		}
	}
}

void Empire::play_game() {
	bool done = false;

	while (!done) {
		md_clr_scr();
		md_sendfile("ansi/overview.ans", 0);
		int width = name.size() + 21;

		md_set_cursor(1, 80 / 2 - width / 2);
		md_printf("`bright yellow`State of `bright white`%s `bright yellow`Turn # `bright white`%d", name.c_str(), total_turns);

		if (total_turns <= turns_in_protection) {
			md_set_cursor(1, 64);
			md_printf(" `bright red`(In Protection)");
		}
		md_set_cursor(3, 14);
		md_printf("`bright white`%u", troops);
		md_set_cursor(4, 14);
		md_printf("`bright white`%u", generals);
		md_set_cursor(5, 14);
		md_printf("`bright white`%u", fighters);
		md_set_cursor(6, 14);
		md_printf("`bright white`%u", defence_stations);
		md_set_cursor(8, 14);

		uint64_t troop_wages = (uint64_t)troops * 10 + (uint64_t)generals * 15 + (uint64_t)fighters * 20 + (uint64_t)defence_stations * 20;
		uint32_t citizen_hunger = ((uint32_t)population / 10) + 1;
		bool can_attack_mapa = (command_ship == 100 && troops >= 100000 && generals >= 10000 && fighters >= 10000);
		md_printf("`bright white`%" PRIu64 "`white`c", troop_wages);

		md_set_cursor(8, 25);
		if (troop_wages > credits) {
			md_printf("`bright yellow`%" PRIu64 "`white`c", credits);
		}
		else {
			md_printf("`bright white`%" PRIu64 "`white`c", troop_wages);
		}

		md_set_cursor(11, 14);
		md_printf("`bright white`%u", population);

		md_set_cursor(13, 14);
		md_printf("`bright white`%u`white`f", citizen_hunger);

		md_set_cursor(13, 25);
		if (citizen_hunger > food) {
			md_printf("`bright yellow`%u`white`f", food);
		}
		else {
			md_printf("`bright white`%u`white`f", citizen_hunger);
		}

		md_set_cursor(16, 9);
		md_printf("`bright white`%u", planets_food);
		md_set_cursor(17, 9);
		md_printf("`bright white`%u", planets_urban);
		md_set_cursor(18, 9);
		md_printf("`bright white`%u", planets_ore);
		md_set_cursor(16, 25);
		md_printf("`bright white`%u", planets_industrial);
		md_set_cursor(17, 25);
		md_printf("`bright white`%u", planets_military);
		md_set_cursor(18, 25);
		md_printf("`bright white`%u", planets_tech);

		md_set_cursor(21, 12);
		md_printf("`bright white`%" PRIu64 "`white`c", credits);
		md_set_cursor(22, 12);
		md_printf("`bright white`%" PRId64 "`white`c", bank_balance);

		int mlevel = get_research_level(researching);

		md_set_cursor(3, 52);
		md_printf("`bright white`%s %d`white`", researching.c_str(), mlevel + 1);
		md_set_cursor(4, 52);
		int mnext_level = (500 * pow((mlevel + 2), 2)) - (500 * (mlevel + 2));
		md_printf("`bright white`%d/%d", tech_points, mnext_level);


		md_set_cursor(7, 56);
		md_printf("`bright white`%u", food);
		md_set_cursor(8, 56);
		md_printf("`bright white`%u", ore);
		md_set_cursor(9, 56);
		md_printf("`bright white`%u", sprockets);
		md_set_cursor(12, 61);
		md_printf("`bright white`%u%%", command_ship);
		md_set_cursor(19, 58);
		md_printf("`bright yellow`(`bright white`%d`bright yellow` Left)", turns_left);
		char answer;

		if (can_attack_mapa && !game_has_been_won) {
			md_set_cursor(16, 62);
			md_printf("`bright cyan`7. `bright white`INSURRECTION!");
			answer = md_get_answer((char*)"1234567tTqQ");
		}
		else {
			answer = md_get_answer((char*)"123456tTqQ");
		}

		switch (tolower(answer)) {
		case '1':
		{
			// manage military
			bool mil_done = false;
			while (!mil_done) {
				md_clr_scr();
				md_sendfile("ansi/mil_hdr.ans", 0);
				md_printf("   `bright white`  Your Credits: %" PRIu64 "`white`c\r\n", credits);
				md_printf("   `bright white`Current Upkeep: %" PRIu64 "`white`c\r\n\r\n", (uint64_t)troops * 10 + (uint64_t)generals * 15 + (uint64_t)fighters * 20 + (uint64_t)defence_stations * 20);

				md_printf("   `bright green`1. `bright white`Troops       `white`(You have: %10u) `bright green`Buy Price: `bright white`100`white`c\r\n", troops);
				md_printf("   `bright green`2. `bright white`Generals     `white`(You have: %10u) `bright green`Buy Price: `bright white`500`white`c\r\n", generals);
				md_printf("   `bright green`3. `bright white`Fighters     `white`(You have: %10u) `bright green`Buy Price: `bright white`1000`white`c\r\n", fighters);
				md_printf("   `bright green`4. `bright white`Turrets      `white`(You have: %10u) `bright green`Buy Price: `bright white`1000`white`c\r\n", defence_stations);

				if (command_ship == 100)
				{
					md_printf("   `bright green`5. `bright white`Command Ship `white`(Progress: `bright geen`complete`white`)\r\n", command_ship);
				}
				else
				{
					md_printf("   `bright green`5. `bright white`Command Ship `white`(Progress: %9u%%) `bright green`Buy Price: `bright white`%u`white`s \r\n", command_ship, 10000 * (command_ship + 1));
				}
				md_printf("   `bright green`0. `bright red`Disband Armies\r\n");
				md_printf("\r\n");
				md_printf("   `bright green`Q`white` Done\r\n");
				char c = md_get_answer((char *)"123450qQ\r");
				uint64_t i;
				char buffer[10];
				switch (c) {
				case '1':
					md_printf("How many troops do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", credits / 100);

					i = gd_get_uint64(credits / 100);
					if (i * 100 > credits)
					{
						md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
					}
					else
					{
						troops += (uint32_t)i;
						credits -= i * 100;
					}
					break;
				case '2':
					md_printf("How many generals do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", credits / 500);
					i = gd_get_uint64(credits / 500);
					if (i * 500 > credits)
					{
						md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
					}
					else
					{
						generals += (uint32_t)i;
						credits -= i * 500;
					}
					break;
				case '3':
					md_printf("How many fighters do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", credits / 1000);
					i = gd_get_uint64(credits / 1000);
					if (i * 1000 > credits)
					{
						md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
					}
					else
					{
						fighters += (uint32_t)i;
						credits -= i * 1000;
					}
					break;
				case '4':
					md_printf("How many turrets do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", credits / 1000);
					i = gd_get_uint64(credits / 1000);

					if (i * 1000 > credits)
					{
						md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
					}
					else
					{
						defence_stations += (uint32_t)i;
						credits -= i * 1000;
					}
					break;
				case '5':
					md_printf("Add to your command ship? (Y/`bright green`N`white`) ");
					c = md_get_answer((char *)"YyNn\r");

					if (c == 'y' || c == 'Y')
					{
						if (command_ship >= 100)
						{
							md_printf("\r\n`bright red`You can't construct any more!`white`\r\n");
						}
						else if ((command_ship + 1) * 10000 > sprockets)
						{
							md_printf("\r\n`bright red`You don't have enough sprockets!`white`\r\n");
						}
						else
						{
							command_ship++;
							sprockets -= command_ship * 10000;
						}
					}
					break;
				case '0':
					md_printf("\r\n\r\n`bright red`Warning: `bright white`You gain no compensation for disbanding armies.`white`\r\n\r\n");
					if (troops > 0)
					{
						md_printf("\r\nDisband how many troops? (`bright green`0`white` - %u) ", troops);
						md_getstring(buffer, 10, '0', '9');
						if (strlen(buffer) != 0)
						{
							i = strtoull(buffer, NULL, 10);
							if (i > troops)
							{
								i = troops;
							}
							troops -= (uint32_t)i;
						}
					}
					if (generals > 0)
					{
						md_printf("\r\nDisband how many generals? (`bright green`0`white` - %u) ", generals);
						md_getstring(buffer, 10, '0', '9');
						if (strlen(buffer) != 0)
						{
							i = strtoull(buffer, NULL, 10);
							if (i > generals)
							{
								i = generals;
							}
							generals -= (uint32_t)i;
						}
					}
					if (fighters > 0)
					{
						md_printf("\r\nDisband how many fighters? (`bright green`0`white` - %u) ", fighters);
						md_getstring(buffer, 10, '0', '9');
						if (strlen(buffer) != 0)
						{
							i = strtoull(buffer, NULL, 10);
							if (i > fighters)
							{
								i = fighters;
							}
							fighters -= (uint32_t)i;
						}
					}
					if (defence_stations > 0)
					{
						md_printf("\r\nDisband how many turrets? (`bright green`0`white` - %u) ", defence_stations);
						md_getstring(buffer, 10, '0', '9');
						if (strlen(buffer) != 0)
						{
							i = strtoull(buffer, NULL, 10);
							if (i >defence_stations)
							{
								i = defence_stations;
							}
							defence_stations -= (uint32_t)i;
						}
					}

					break;
				default:
					mil_done = true;
				}
			}
			save();
		}
		break;
		case '2':
		{
			// manage commodities
			bool done = false;
			while (!done) {
				md_clr_scr();
				md_sendfile("ansi/com_hdr.ans", 0);
				md_printf("   `bright white`  Your Credits: %" PRIu64 "`white`c\r\n", credits);
				md_printf("   `bright white`Current Upkeep: %" PRIu64 "`white`c\r\n\r\n", (uint64_t)troops * 10 + (uint64_t)generals * 15 + (uint64_t)fighters * 20 + (uint64_t)defence_stations * 20);
				md_printf("   `bright blue`1. `bright white`Food      `white`(You have: %10u) `bright green`Buy Price:  `bright white`10, `bright red`Sell Price:  `bright white`5\r\n", food);
				md_printf("   `bright blue`2. `bright white`Ore       `white`(You have: %10u) `bright green`Buy Price:  `bright white`10, `bright red`Sell Price:  `bright white`5\r\n", ore);
				md_printf("   `bright blue`3. `bright white`Sprockets `white`(You have: %10u) `bright green`Buy Price:  `bright white`20, `bright red`Sell Price: `bright white`10\r\n", sprockets);
				md_printf("   `bright blue`Q. `bright white`Done\r\n");
				char c = md_get_answer((char*)"123qQ");
				md_printf("\r\n");
				
				char bs;

				switch (c) {
				case '1':
					md_printf("\r\n`bright blue`B`bright white`uy or `bright blue`S`bright white`ell? ");
					bs = md_get_answer((char*)"bBsS");
					md_printf("\r\n");

					if (tolower(bs) == 's' && food == 0) {
						md_printf("\r\n`bright red`You don't have any to sell!");
					}
					else if (tolower(bs) == 's') {
						md_printf("\r\n`white`How many `bright white`food`white` do you want to sell (max %u) ? ", food);
						uint32_t sellamt = (uint32_t)gd_get_uint64(food);

						if (sellamt <= food) {
							food -= sellamt;
							credits += (uint64_t)sellamt * 5;
							save();
						}
						else {
							md_printf("\r\n`bright red`You don't have that many!");
						}
					}
					else if (tolower(bs) == 'b') {
						md_printf("\r\n`white`How many `bright white`food`white` do you want to buy (max %u) ? ", credits / 10);
						uint32_t buyamt = (uint32_t)gd_get_uint64(credits / 10);

						if ((uint64_t)buyamt * 10 <= credits) {
							food += buyamt;
							credits -= (uint64_t)buyamt * 10;
							save();
						}
						else {
							md_printf("\r\n`bright red`You don't have enough credits!");
						}
					}
					break;
				case '2':
					md_printf("\r\n`bright blue`B`bright white`uy or `bright blue`S`bright white`ell? ");
					bs = md_get_answer((char*)"bBsS");
					md_printf("\r\n");

					if (tolower(bs) == 's' && ore == 0) {
						md_printf("\r\n`bright red`You don't have any to sell!");
					}
					else if (tolower(bs) == 's') {
						md_printf("\r\n`white`How many `bright white`ore`white` do you want to sell (max %u) ? ", ore);
						uint32_t sellamt = (uint32_t)gd_get_uint64(ore);

						if (sellamt <= ore) {
							ore -= sellamt;
							credits += (uint64_t)sellamt * 5;
							save();
						}
						else {
							md_printf("\r\n`bright red`You don't have that many!");
						}
					}
					else if (tolower(bs) == 'b') {
						md_printf("\r\n`white`How many `bright white`ore`white` do you want to buy (max %u) ? ", credits / 10);
						uint32_t buyamt = (uint32_t)gd_get_uint64(credits / 10);

						if ((uint64_t)buyamt * 10 <= credits) {
							ore += buyamt;
							credits -= (uint64_t)buyamt * 10;
							save();
						}
						else {
							md_printf("\r\n`bright red`You don't have enough credits!");
						}
					}
					break;
				case '3':
					md_printf("`bright blue`B`bright white`uy or `bright blue`S`bright white`ell? ");
					bs = md_get_answer((char*)"bBsS");
					md_printf("\r\n");

					if (tolower(bs) == 's' && sprockets == 0) {
						md_printf("\r\n`bright red`You don't have any to sell!");
					}
					else if (tolower(bs) == 's') {
						md_printf("\r\n`white`How many `bright white`sprockets`white` do you want to sell (max %u) ? ", sprockets);
						uint32_t sellamt = (uint32_t)gd_get_uint64(sprockets);

						if (sellamt <= sprockets) {
							sprockets -= sellamt;
							credits += (uint64_t)sellamt * 10;
							save();
						}
						else {
							md_printf("\r\n`bright red`You don't have that many!");
						}
					}
					else if (tolower(bs) == 'b') {
						md_printf("`white`How many `bright white`sprockets`white` do you want to buy (max %u) ? ", credits / 20);
						uint32_t buyamt = (uint32_t)gd_get_uint64(credits / 20);

						if ((uint64_t)buyamt * 20 <= credits) {
							sprockets += buyamt;
							credits -= (uint64_t)buyamt * 10;
							save();
						}
						else {
							md_printf("\r\n`bright red`You don't have enough credits!");
						}
					}
					break;
				default:
					done = true;
					break;
				}
				if (!done) {
					md_printf("\r\n`bright white`Press any key to continue...`white`");
					md_getc();
				}
			}
		}
		break;
		case '3':
		{
			bool done = false;

			while (!done) {
				md_clr_scr();
				md_sendfile("ansi/exp_hdr.ans", 0);
				md_printf("   `bright white`  Your Credits: %" PRIu64 "`white`c\r\n", credits);
				md_printf("   `bright white`Current Upkeep: %" PRIu64 "`white`c\r\n\r\n", (uint64_t)troops * 10 + (uint64_t)generals * 15 + (uint64_t)fighters * 20 + (uint64_t)defence_stations * 20);

				md_printf("   `white`What kind of planets are you looking for?\r\n\r\n");
				// exploration

				md_printf("   `bright yellow`  1. `bright white`Mining        `white`(You have: %u)\r\n", planets_ore);
				md_printf("   `bright yellow`  2. `bright white`Farming       `white`(You have: %u)\r\n", planets_food);
				md_printf("   `bright yellow`  3. `bright white`Military      `white`(You have: %u)\r\n", planets_military);
				md_printf("   `bright yellow`  4. `bright white`Industrial    `white`(You have: %u)\r\n", planets_industrial);
				md_printf("   `bright yellow`  5. `bright white`Urban         `white`(You have: %u)\r\n", planets_urban);
				md_printf("   `bright yellow`  6. `bright white`Technology    `white`(You have: %u)\r\n", planets_tech);				
				md_printf("   `bright yellow`  Q. `bright white`Done\r\n");
				char c = md_get_answer((char*)"123456qQ");
				char buffer[9];
				md_printf("\r\n");
				switch (c)
				{
				case '1':
					md_printf("How many mining planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						int64_t i = strtoull(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " mining planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + ((uint64_t)planets_ore + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", credits);
							c = md_get_answer((char*)"YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (credits < i * 2000 + ((uint64_t)planets_ore + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									credits -= ((i * 2000) + (((uint64_t)planets_ore + i) * 100));
									planets_ore += (uint32_t)i;
									save();
								}
							}
						}
					}
					break;
				case '2':
					md_printf("How many farming planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						int64_t i = strtoull(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " farming planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + ((uint64_t)planets_food + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", credits);
							c = md_get_answer((char*)"YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (credits < i * 2000 + ((uint64_t)planets_food + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									credits -= ((i * 2000) + (((uint64_t)planets_food + i) * 100));
									planets_food += (uint32_t)i;
									save();
								}
							}
						}
					}
					break;
				case '3':
					md_printf("How many military planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						int64_t i = strtoull(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " military planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + ((uint64_t)planets_military + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", credits);
							c = md_get_answer((char*)"YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (credits < i * 2000 + ((uint64_t)planets_military + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									credits -= ((i * 2000) + (((uint64_t)planets_military + i) * 100));
									planets_military += (uint32_t)i;
									save();
								}
							}
						}
					}
					break;
				case '4':
					md_printf("How many industrial planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						int64_t i = strtoull(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " industrial planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + ((uint64_t)planets_industrial + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", credits);
							c = md_get_answer((char*)"YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (credits < i * 2000 + ((uint64_t)planets_industrial + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									credits -= ((i * 2000) + (((uint64_t)planets_industrial + i) * 100));
									planets_industrial += (uint32_t)i;
									save();
								}
							}
						}
					}
					break;
				case '5':
					md_printf("How many urban planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						int64_t i = strtoull(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " urban planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + ((uint64_t)planets_urban + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", credits);
							c = md_get_answer((char*)"YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (credits < i * 2000 + ((uint64_t)planets_urban + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									credits -= ((i * 2000) + (((uint64_t)planets_urban + i) * 100));
									planets_urban += (uint32_t)i;
									save();
								}
							}
						}
					}
					break;
				case '6':
					md_printf("How many technology planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						int64_t i = strtoull(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " technology planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + ((uint64_t)planets_tech + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", credits);
							c = md_get_answer((char*)"YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (credits < i * 2000 + ((uint64_t)planets_tech + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									credits -= ((i * 2000) + (((uint64_t)planets_tech + i) * 100));
									planets_tech += (uint32_t)i;
									save();
								}
							}
						}
					}
					break;					
				default:
					done = true;
					break;
				}
				
			}
		}
		break;
		case '4':
		{
			// Bank
			bool done = false;
			while (!done) {
				md_clr_scr();
				md_sendfile("ansi/bnk_hdr.ans", 0);
				md_printf("   `bright white`  Your Credits: %" PRIu64 "`white`c\r\n", credits);
				md_printf("   `bright white`Current Upkeep: %" PRIu64 "`white`c\r\n\r\n", (uint64_t)troops * 10 + (uint64_t)generals * 15 + (uint64_t)fighters * 20 + (uint64_t)defence_stations * 20);
				if (bank_balance < 0) {
					md_printf("   `bright white`  Bank Balance: `bright red`%" PRId64 "`white`c\r\n", bank_balance);
				}
				else {
					md_printf("   `bright white`  Bank Balance: `bright green`%" PRId64 "`white`c\r\n", bank_balance);
				}
				md_printf("\r\n");
				md_printf("    Your current allowed overdraft is %" PRIu64 " credits.\r\n\r\n", (calculate_score() * 100) / 2);
				

				md_printf("   `bright cyan`  1. `bright white`Deposit\r\n");
				md_printf("   `bright cyan`  2. `bright white`Widthdraw\r\n");
				md_printf("   `bright cyan`  Q. `bright white`Done\r\n");
				char c = md_get_answer((char*)"12qQ");
				switch (c) {
				case '1':
				{
					md_printf("\r\n\r\nHow much would you like to deposit (0 - %" PRIu64 " credits) ? ", credits);
					uint64_t i = gd_get_uint64(credits);
					if (i > credits)
					{
						md_printf("\r\n`bright red`You don't have that many credits!`white`\r\n");
					}
					else
					{
						credits -= i;
						bank_balance += i;
						save();
					}
				}
					break;
				case '2':
				{
					md_printf("\r\n\r\nHow much would you like to withdraw (0 - %" PRIu64 " credits) ? ", bank_balance + ((calculate_score() * 100) / 2));
					uint64_t i = gd_get_uint64(bank_balance + ((calculate_score() * 100) / 2));
					if (i > bank_balance + ((calculate_score() * 100) / 2))
					{
						md_printf("\r\n`bright red`You don't have that many credits!`white`\r\n");
					}
					else
					{
						bank_balance -= i;
						credits += i;
						save();
					}
					break;

				}
				default:
					done = true;
				}
			}
		}
		break;
		case '5':
		{
			bool done = false;
			while (!done) {
				// Diplomatic Relations
				md_clr_scr();
				md_sendfile("ansi/dip_hdr.ans", 0);
				md_printf("   `bright white`  Your Credits: %" PRIu64 "`white`c\r\n", credits);
				md_printf("   `bright white`Current Upkeep: %" PRIu64 "`white`c\r\n\r\n", (uint64_t)troops * 10 + (uint64_t)generals * 15 + (uint64_t)fighters * 20 + (uint64_t)defence_stations * 20);
				md_printf("   `bright magenta`  1. `bright white`Messaging System\r\n");
				md_printf("   `bright magenta`  2. `bright white`Launch an Attack\r\n");
				md_printf("   `bright magenta`  3. `bright white`Recruit Spies       `white`(You have: %10u) `bright green`Buy Price: `bright white`5000`white`c\r\n", spies);
				md_printf("   `bright magenta`  4. `bright white`Spy on an Empire                                `bright green`Cost: `bright white`1000`white`c\r\n");
				md_printf("   `bright magenta`  5. `bright white`Sabotage an Empire                              `bright green`Cost: `bright white`1000`white`c\r\n");
				md_printf("   `bright magenta`  Q. `bright white`Done\r\n");
				char c = md_get_answer((char*)"12345qQ");
				md_printf("\r\n");
				switch (c) {
				case '1':
					show_messages();
					break;
				case '2':
					if (total_turns > turns_in_protection) {
						if (troops > 0) {
							char buffer[16];
							while (true) {
								md_printf("`bright magenta`What empire do you want to attack? (? to list): `white`");
								if (md_getstring(buffer, 16, 32, 126) > 0) {
									if (buffer[0] == '?' && strlen(buffer) == 1) {
										if (list_empires(id) == 0) {
											md_printf("\r\n`bright red`No other empires exist!`white`\r\n");
											break;
										}
									}
									else {
										sqlite3* db;
										sqlite3_stmt* stmt;
										static const char* sql = "SELECT id FROM empires WHERE empname = ?";

										if (!open_database(&db)) {
											break;
										}
										if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
											sqlite3_close(db);
											break;
										}
										sqlite3_bind_text(stmt, 1, buffer, -1, NULL);
										if (sqlite3_step(stmt) == SQLITE_ROW) {
											int mid = sqlite3_column_int(stmt, 0);
											sqlite3_finalize(stmt);
											sqlite3_close(db);
											// do attack
											uint32_t tattack = 0;
											uint32_t gattack = 0;
											uint32_t fattack = 0;

											md_printf("\r\n`bright white`Send how many troops? (You have %u): ", troops);
											tattack = (uint32_t)gd_get_uint64(troops);

											if (tattack == 0) {
												md_printf("\r\n`bright red`You need to send some troops!`white`\r\n");
											}
											else {
												if (generals > 0) {
													md_printf("\r\n`bright white`Send how many generals? (You have %u): ", generals);
													gattack = (uint32_t)gd_get_uint64(generals);
												}

												if (fighters > 0) {
													md_printf("\r\n`bright white`Send how many fighters? (You have %u): ", fighters);
													fattack = (uint32_t)gd_get_uint64(fighters);
												}

												troops -= tattack;
												generals -= gattack;
												fighters -= fattack;

												do_battle(mid, &tattack, &gattack, &fattack);

												troops += tattack;
												generals += gattack;
												fighters += fattack;

												save();
											}

											break;
										}
										else {
											sqlite3_finalize(stmt);
											sqlite3_close(db);
											md_printf("\r\n`bright red`No other empires by that name!`white`\r\n");
											break;
										}
									}
								}
								else {
									break;
								}
							}
						}
						else {
							md_printf("\r\n`bright red`You have no troops!\r\n");
						}
					}
					else {
						md_printf("\r\n`bright yellow`Sorry, you are currently under protection and can not attack!`white`\r\n");
					}
					md_printf("\r\n\r\n`bright white`Press any key to continue...`white`");
					md_getc();
					break;
				case '3':
					// recruit spies
				{
					md_printf("How many spies do you want to recruit? (MAX `bright yellow`%" PRIu64 "`white`) ", credits / 5000);
					uint32_t i = (uint32_t)gd_get_uint64(credits / 5000);
					if (i * 5000 > credits)
					{
						md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
					}
					else
					{
						spies += i;
						credits -= i * 5000;
					}
				}
					md_printf("\r\n`bright white`Press any key to continue....");
					md_getc();
					break;
				case '4':
					// spy on an empire
					if (credits < 1000) {
						md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
					}
					else if (total_turns <= turns_in_protection) {
						md_printf("\r\n`bright yellow`Sorry, you are currently under protection and can not use espionage!`white`\r\n");
					}
					else {
						char buffer[16];
						while (true) {
							md_printf("`bright magenta`What empire do you want to spy on? (? to list): `white`");
							if (md_getstring(buffer, 16, 32, 126) > 0) {
								if (buffer[0] == '?' && strlen(buffer) == 1) {
									if (list_empires(id) == 0) {
										md_printf("\r\n`bright red`No other empires exist!`white`\r\n");
										break;
									}
								}
								else {
									sqlite3* db;
									sqlite3_stmt* stmt;
									static const char* sql = "SELECT id FROM empires WHERE empname = ?";

									if (!open_database(&db)) {
										break;
									}
									if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
										sqlite3_close(db);
										break;
									}
									sqlite3_bind_text(stmt, 1, buffer, -1, NULL);
									if (sqlite3_step(stmt) == SQLITE_ROW) {
										int mid = sqlite3_column_int(stmt, 0);
										sqlite3_finalize(stmt);
										sqlite3_close(db);
										int i = rand() % 100 + 1;
										credits -= 1000;
										if (i < 50)
										{
											md_printf("\r\nYour spy was caught and executed!\r\n");
											spies--;
											send_message(mid, "SYSTEM", "A spy from " + name + "was caught trying to infiltrate your empire!");
											save();
										}
										else
										{
											save();
											Empire otheremp;
											md_printf("\r\nYour spy was successful!\r\n");

											otheremp.load(mid);

											while (true) {
												md_printf("\r\nWhat should your spy infiltrate?\r\n");
												md_printf(" 1. Assess Military\r\n");
												md_printf(" 2. Assess Planetary Holdings\r\n");
												md_printf(" 3. Assess Finances\r\n");

												char c = md_getc();

												if (c == '1') {
													md_printf("Spy's report `bright green`Military`white`\r\n\r\n");
													md_printf("  - Troop count   : %u\r\n", otheremp.troops);
													md_printf("  - General count : %u\r\n", otheremp.generals);
													md_printf("  - Fighter count : %u\r\n", otheremp.fighters);
													md_printf("  - Turret count  : %u\r\n", otheremp.defence_stations);
													md_printf("  - Command Ship  : %u%% Complete\r\n", otheremp.command_ship);
													break;
												}
												else if (c == '2') {
													md_printf("Spy's report `bright yellow`Planetary`white`\r\n\r\n");
													md_printf("  - Farming   : %u\r\n", otheremp.planets_food);
													md_printf("  - Urban     : %u\r\n", otheremp.planets_urban);
													md_printf("  - Mining    : %u\r\n", otheremp.planets_ore);
													md_printf("  - Industrial: %u\r\n", otheremp.planets_industrial);
													md_printf("  - Military  : %u\r\n", otheremp.planets_military);
													md_printf("  - Technology: %u\r\n", otheremp.planets_tech);
													break;
												}
												else if (c == '3') {
													md_printf("Spy's report `bright cyan`Financial`white`\r\n\r\n");
													md_printf("  - Credits on Hand : %" PRIu64 "\r\n", otheremp.credits);
													md_printf("  - Credits Banked  : %" PRId64 "\r\n", otheremp.bank_balance);
													md_printf("  - Food Stores     : %u\r\n", otheremp.food);
													md_printf("  - Ore Stores      : %u\r\n", otheremp.ore);
													md_printf("  - Sprocket Stores : %u\r\n", otheremp.sprockets);
													break;
												}
											}
											break;
										}
									}
									else {
										sqlite3_finalize(stmt);
										sqlite3_close(db);
										md_printf("\r\n`bright red`No other empires by that name!`white`\r\n");
										break;
									}
								}
							}
						}
					}
					md_printf("\r\n`bright white`Press any key to continue....");
					md_getc();
					break;
				case '5':
					//sabotage an empire
					if (credits < 1000) {
						md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
					}
					else if (total_turns <= turns_in_protection) {
						md_printf("\r\n`bright yellow`Sorry, you are currently under protection and can not use espionage!`white`\r\n");
					}
					else {
						char buffer[16];
						while (true) {
							md_printf("`bright magenta`What empire do you want to sabotage on? (? to list): `white`");
							if (md_getstring(buffer, 16, 32, 126) > 0) {
								if (buffer[0] == '?' && strlen(buffer) == 1) {
									if (list_empires(id) == 0) {
										md_printf("\r\n`bright red`No other empires exist!`white`\r\n");
										break;
									}
								}
								else {
									sqlite3* db;
									sqlite3_stmt* stmt;
									static const char* sql = "SELECT id FROM empires WHERE empname = ?";

									if (!open_database(&db)) {
										break;
									}
									if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
										sqlite3_close(db);
										break;
									}
									sqlite3_bind_text(stmt, 1, buffer, -1, NULL);
									if (sqlite3_step(stmt) == SQLITE_ROW) {
										int mid = sqlite3_column_int(stmt, 0);
										sqlite3_finalize(stmt);
										sqlite3_close(db);
										int i = rand() % 100 + 1;
										credits -= 1000;
										if (i < 50)
										{
											md_printf("\r\nYour spy was caught and executed!\r\n");
											spies--;
											send_message(mid, "SYSTEM", "A spy from " + name + "was caught trying to sabotage your empire!");
											save();
										}
										else
										{
											save();
											Empire otheremp;
											md_printf("\r\nYour spy was successful!\r\n");

											otheremp.load(mid);

											while (true) {
												md_printf("\r\nWhat should your spy sabotage?\r\n");
												md_printf(" 1. Demoralize Military\r\n");
												md_printf(" 2. Planetary Biological Attack\r\n");
												md_printf(" 3. Market Heist\r\n");

												char c = md_getc();

												if (c == '1') {
													md_printf("Spy's report `bright green`Military`white`\r\n\r\n");

													int type = rand() % 4 + 1;
													int amount = rand() % 25 + 1;
													uint32_t gone;

													switch (type) {
													case 1:
														gone = (uint32_t)(otheremp.troops * ((double)amount / 100.));

														otheremp.troops -= gone;

														md_printf("%u troops were convinced to desert.", gone);
														otheremp.save();
														break;
													case 2:
														gone = (uint32_t)(otheremp.generals * ((double)amount / 100.));

														otheremp.generals -= gone;

														md_printf("%u generals were convinced to desert.", gone);
														otheremp.save();
														break;
													case 3:
														gone = (uint32_t)(otheremp.fighters * ((double)amount / 100.));

														otheremp.fighters -= gone;

														md_printf("%u fighters were sabotaged.", gone);
														otheremp.save();
														break;
													case 4:
														gone = (uint32_t)(otheremp.defence_stations * ((double)amount / 100.));

														otheremp.defence_stations -= gone;

														md_printf("%u turrets were sabotaged.", gone);
														otheremp.save();
														break;
													}

													break;
												}
												else if (c == '2') {
													int type = rand() % 6 + 1;
													int amount = rand() % 10 + 1;
													md_printf("Spy's report `bright yellow`Planetary`white`\r\n\r\n");
													uint32_t gone;

													switch (type) {
													case 1:
														gone = (uint32_t)(otheremp.planets_food * ((double)amount / 100.));

														otheremp.planets_food -= gone;

														md_printf("%u farming planets were rendered useless.", gone);
														otheremp.save();
														break;
													case 2:
														gone = (uint32_t)(otheremp.planets_ore * ((double)amount / 100.));

														otheremp.planets_ore -= gone;

														md_printf("%u mining planets were rendered useless.", gone);
														otheremp.save();
														break;
													case 3:
														gone = (uint32_t)(otheremp.planets_urban * ((double)amount / 100.));

														otheremp.planets_urban -= gone;

														md_printf("%u urban planets were rendered useless.", gone);
														otheremp.save();
														break;
													case 4:
														gone = (uint32_t)(otheremp.planets_industrial * ((double)amount / 100.));

														otheremp.planets_industrial -= gone;

														md_printf("%u industrial planets were rendered useless.", gone);
														otheremp.save();
														break;
													case 5:
														gone = (uint32_t)(otheremp.planets_military * ((double)amount / 100.));

														otheremp.planets_military -= gone;

														md_printf("%u military planets were rendered useless.", gone);
														otheremp.save();
														break;
													case 6:
														gone = (uint32_t)(otheremp.planets_tech * ((double)amount / 100.));

														otheremp.planets_tech -= gone;

														md_printf("%u technology planets were rendered useless.", gone);
														otheremp.save();
														break;														
													}
													break;
												}
												else if (c == '3') {
													md_printf("Spy's report `bright cyan`Financial`white`\r\n\r\n");

													int type = rand() % 4 + 1;
													int amount = rand() % 25 + 1;
													uint32_t gone;

													switch (type) {
													case 1:
														gone = (uint32_t)(otheremp.credits * ((double)amount / 100.));

														otheremp.credits -= gone;

														md_printf("%u credits were destroyed.", gone);
														otheremp.save();
														break;
													case 2:
														gone = (uint32_t)(otheremp.food * ((double)amount / 100.));

														otheremp.food -= gone;

														md_printf("%u food was destroyed.", gone);
														otheremp.save();
														break;
													case 3:
														gone = (uint32_t)(otheremp.ore * ((double)amount / 100.));

														otheremp.ore -= gone;

														md_printf("%u ore was destoyed.", gone);
														otheremp.save();
														break;
													case 4:
														gone = (uint32_t)(otheremp.sprockets * ((double)amount / 100.));

														otheremp.sprockets -= gone;

														md_printf("%u sprockets were destroyed.", gone);
														otheremp.save();
														break;
													}
													break;
												}
											}
											break;
										}
									}
									else {
										sqlite3_finalize(stmt);
										sqlite3_close(db);
										md_printf("\r\n`bright red`No other empires by that name!`white`\r\n");
										break;
									}
								}
							}
						}
					}
					md_printf("\r\n`bright white`Press any key to continue....");
					md_getc();
					break;
				default:
					done = true;
					break;
				}
			}
		}
		break;
		case '6':
		{
			bool done = false;
			while (!done) {
				// Diplomatic Relations
				md_clr_scr();
				md_sendfile("ansi/res_hdr.ans", 0);
				md_printf(" `bright white`You are currently have `magenta`%d `bright white`Tech Points towards `magenta`%s %d`bright white`\r\n\r\n", tech_points, researching.c_str(), get_research_level(researching) + 1);
				
				md_printf(" `bright white`Do you want to change your research focus?\r\n `white`(`bright red`WARNING: `white`All current tech points will be lost)\r\n\r\n");
				md_printf("   `magenta`  1. `bright white`Agriculture %d\r\n", get_research_level("Agriculture") + 1);
				md_printf("   `magenta`  2. `bright white`Urban Planning %d\r\n", get_research_level("Urban Planning") + 1);
				md_printf("   `magenta`  3. `bright white`Military Tactics %d\r\n", get_research_level("Military Tactics") + 1);
				md_printf("   `magenta`  4. `bright white`Geology %d\r\n", get_research_level("Geology") + 1);
				md_printf("   `magenta`  5. `bright white`Manufacturing %d\r\n", get_research_level("Manufacturing") + 1);
				md_printf("   `magenta`  Q. `bright white`Done\r\n");

				char c = md_get_answer((char*)"12345qQ");
				md_printf("\r\n");

				switch (c) {
					case '1':
						researching = "Agriculture";
						tech_points = 0;
						break;
					case '2':
						researching = "Urban Planning";
						tech_points = 0;
						break;
					case '3':
						researching = "Military Tactics";
						tech_points = 0;
						break;
					case '4':
						researching = "Geology";
						tech_points = 0;
						break;
					case '5':
						researching = "Manufactureing";
						tech_points = 0;
						break;
					default:
						done = true;
						break;
				}
			}
		}
		break;
		case '7':
		{
			// Overthrow system lord
			md_printf("You have amassed a mighty force, launch an attack on `bright magenta`Overlord Mapa`white`? (Y/`bright green`N) ");
			char c = md_get_answer((char *)"YyNn\r");
			if (tolower(c) == 'y')
			{
				int i = rand() % 100;

				if (i < 20)
				{
					// you win!
					md_printf("\r\n\r\n`bright green`You are victorious! Overlord Mapa has been defeated\r\nand all the universe hails you as her heir!`white`\r\n\r\n");
					md_printf("`bright white`You have won the game!\r\n");
					game_won();
				}
				else
				{
					md_printf("\r\n\r\n`bright red`You are defeated. Your invasion force has been obliterated.`white`\r\n");

					command_ship = 0;
					troops -= 100000;
					generals -= 10000;
					fighters -= 10000;
				}
			}

		}
		break;
		case 't':
		{
			// take turn
			if (turns_left > 0) {
				md_clr_scr();
				int32_t total_ore;
				int32_t total_industrial;
				int32_t total_tech;
				int32_t total_military;
				int32_t total_food;
				uint32_t unemployment;
				
				int level = get_research_level(researching);

				md_clr_scr();
				md_printf("`bright yellow`Turn #`bright white`%d `bright yellow`Summary...`white`\r\n\r\n", total_turns);



				double starvation = (double)food / (double)citizen_hunger;
				double loyalty = (double)credits / (double)troop_wages;
				// Population Changes
				if (starvation < 1.0)
				{
					md_printf("`bright red`%u `white`citizens died from starvation\r\n", (uint32_t)(population - ((double)population * starvation)));
					population -= (population - (uint32_t)((double)population * starvation));
                    food = 0;
				}
				else
				{
					float density = 50.f - (get_research_level("Urban Planning"));

					if (population > (int)((float)planets_urban * density))
					{
						md_printf("`bright yellow`No population increase due to not enough urban planets`white`\r\n");
					}
					else
					{
						md_printf("`bright white`%u `white`new citizens join the empire\r\n", (uint32_t)((double)population * 0.05));
						population += (uint32_t)((double)population * 0.05);
					}
					food -= citizen_hunger;
				}

				if (loyalty < 1.0)
				{
					md_printf("`bright red`%u `white`troops fled the empire\r\n", (uint32_t)(troops - ((double)troops * loyalty)));
					troops -= (troops - (uint32_t)((double)troops * loyalty));
                    credits = 0;
				} else {
                    credits -= troop_wages;
                }

				unemployment = population;

				if (planets_food > 0)
				{
					float bonus = get_research_level("Agriculture") / 20.f;
					float prod = 10.f + bonus;

					int capacity = planets_food * 10;
					int employed = 0;

					if (planets_food * 20 > unemployment)
					{
						total_food = (int)((float)planets_food * prod) - (unemployment / 20);
						employed = unemployment;
						unemployment = 0;
					}
					else
					{
						total_food = (int)((float)planets_food * prod);
						unemployment -= planets_food * 20;
						employed = planets_food * 20;
					}
					md_printf("Your farming planets produced `bright white`%u `white`food. (Bonus %.3f)\r\n", total_food, bonus);
					md_printf("   - Citizens Employed: %d, Production Capacity: %d/%d\r\n", employed, total_food, capacity);

                    if (total_food > 0) {
                        food += total_food;
                    }
				}

				if (planets_military > 0)
				{
					float bonus = get_research_level("Military Tactics") / 20.f;
					float prod = 10.f + bonus;
					int capacity = planets_military * 10;
					int employed = 0;

					if (planets_military * 20 > unemployment)
					{
						total_military = (int)((float)planets_military * prod) - (unemployment / 20);
						employed = unemployment;
						unemployment = 0;
					}
					else
					{
						total_military = (int)((float)planets_military * prod);
						unemployment -= planets_military * 20;
						employed = planets_military * 20;
					}


					md_printf("Your military planets trained `bright white`%u`white` troops (Bonus: %.3f)\r\n", total_military, bonus);
					md_printf("   - Citizens Employed: %d, Production Capacity: %d/%d\r\n", employed, total_military, capacity);
                    if (total_military > 0) {
                        troops += total_military;
                    }
				}

				if (planets_ore > 0)
				{
					float bonus = get_research_level("Geology") / 20.f;
					float prod = 10.f + bonus;
					int capacity = planets_ore * 10;
					int employed = 0;

					if (planets_ore * 20 > unemployment)
					{
						total_ore = (int)((float)planets_ore * prod) - (unemployment / 20);
						employed = unemployment;
						unemployment = 0;
					}
					else
					{
						total_ore = (int)((float)planets_ore * prod);
						unemployment -= planets_ore * 20;
						employed = planets_ore * 20;
					}


					md_printf("Your mining planets produced `bright white`%u`white` ore. (Bonus: %.3f)\r\n", total_ore, bonus);
					md_printf("   - Citizens Employed: %d, Production Capacity: %d/%d\r\n", employed, total_ore, capacity);

                    if (total_ore > 0) {
                        ore += total_ore;
                    }
				}

				if (planets_industrial > 0)
				{
					float bonus = get_research_level("Manufacturing") / 20.f;
					float prod = 10.f + bonus;
					int capacity = planets_industrial * 10;
					int employed = 0;

					if (planets_industrial * 20 > unemployment)
					{
						total_industrial = (int)((float)planets_industrial * prod) - (unemployment / 20);
						employed = unemployment;
						unemployment = 0;
					}
					else
					{
						total_industrial = (int)((float)planets_industrial * prod);
						unemployment -= planets_industrial * 20;
						employed = planets_industrial * 20;
					}


					if (total_industrial > ore) {
                        total_industrial = ore;
                    }

                    if (total_industrial > 0) {
                        ore -= total_industrial;
                    }

					md_printf("Your industrial planets produced `bright white`%u`white` sprocket. (Bonus: %.3f)\r\n", total_industrial, bonus);
					md_printf("   - You are left with %u ore\r\n", ore);
					md_printf("   - Citizens Employed: %d, Production Capacity: %d/%d\r\n", employed, total_industrial, capacity);
                    if (total_industrial > 0) {
                        sprockets += total_industrial;
                    }
				}

				if (planets_tech > 0)
				{
					int capacity = planets_tech * 10;
					int employed = 0;

					if (planets_tech * 20 > unemployment)
					{
						total_tech = (int)((float)planets_tech * 10) - (unemployment / 20);
						employed = unemployment;
						unemployment = 0;
					}
					else
					{
						total_tech = planets_tech * 10;
						unemployment -= planets_tech * 20;
						employed = planets_tech * 20;
					}


					md_printf("Your technology planets produced `bright white`%u`white` research points.\r\n", total_tech);
					md_printf("   - Citizens Employed: %d, Production Capacity: %d/%d\r\n", employed, total_tech, capacity);
                    if (total_tech > 0) {
                        tech_points += total_tech;
                    }

					
					int next_level = (500 * pow((level + 2), 2)) - (500 * (level + 2));

					if (next_level <= tech_points) {
						set_research_level(researching, level + 1);
						md_printf("\r\nYou have completed researching %s level %d!\r\n", researching.c_str(), level + 1);
						tech_points -= next_level;
					}

				}

				md_printf("\r\nTaxes produce `bright white`%u`white` credits\r\n", (population - unemployment) * 10);
				md_printf("Unemployment: `bright white`%f%%`white`\r\n\r\n", (float)unemployment / (float)population * 100.f);
				credits += ((uint64_t)(population - unemployment) * 10);
				turns_left--;
				total_turns++;
				save();
				md_printf("\r\n\r\n`white`Press any key to continue...");
				md_getc();

			}
		}
		break;
		case 'q':
			// quit game
			done = true;
			break;
		}

	}
}

void Empire::save() {
	sqlite3* db;
	sqlite3_stmt* stmt;
	static const char *savesql = "UPDATE empires SET troops=?, generals=?, fighters=?, defencestations=?, spies=?, population=?, food=?, credits=?, bankbalance=?, sprockets=?, ore=?, tech_points=?, planets_food=?, planets_ore=?,"
		"planets_industrial=?, planets_military=?, planets_urban=?, planets_tech=?, commandship=?, lastplayed=?, turnsleft=?, totalturns=?, researching=? WHERE id = ?";
	if (!open_database(&db)) {
		md_printf("\r\n`bright red`Save failed! Unable to open database`white`\r\n");
		return;
	}

	if (sqlite3_prepare_v2(db, savesql, -1, &stmt, NULL) != SQLITE_OK) {

		return;
	}
	sqlite3_bind_int(stmt, 1, troops);
	sqlite3_bind_int(stmt, 2, generals);
	sqlite3_bind_int(stmt, 3, fighters);
	sqlite3_bind_int(stmt, 4, defence_stations);
	sqlite3_bind_int(stmt, 5, spies);
	sqlite3_bind_int(stmt, 6, population);
	sqlite3_bind_int(stmt, 7, food);
	sqlite3_bind_int64(stmt, 8, credits);
	sqlite3_bind_int64(stmt, 9, bank_balance);
	sqlite3_bind_int(stmt, 10, sprockets);
	sqlite3_bind_int(stmt, 11, ore);
	sqlite3_bind_int(stmt, 12, tech_points);
	sqlite3_bind_int(stmt, 13, planets_food);
	sqlite3_bind_int(stmt, 14, planets_ore);
	sqlite3_bind_int(stmt, 15, planets_industrial);
	sqlite3_bind_int(stmt, 16, planets_military);
	sqlite3_bind_int(stmt, 17, planets_urban);
	sqlite3_bind_int(stmt, 18, planets_tech);
	sqlite3_bind_int(stmt, 19, command_ship);
	sqlite3_bind_int64(stmt, 20, last_played);
	sqlite3_bind_int(stmt, 21, turns_left);
	sqlite3_bind_int(stmt, 22, total_turns);
	sqlite3_bind_text(stmt, 23, researching.c_str(), -1, NULL);
	sqlite3_bind_int64(stmt, 24, id);


	sqlite3_step(stmt);
	sqlite3_finalize(stmt);
	sqlite3_close(db);
	return;
}

int Empire::load(int64_t eid) {
	sqlite3* db;
	sqlite3_stmt* stmt;
	static const char* selectsql = "SELECT id, bbsname, empname, troops, generals, fighters, defencestations, spies, population, food, credits, bankbalance, sprockets, ore, tech_points, planets_food, planets_ore,"
		"planets_industrial, planets_military, planets_urban, planets_tech, commandship, lastplayed, turnsleft, totalturns, researching FROM empires where id = ?";
	if (!open_database(&db)) {
		return -1;
	}

	if (sqlite3_prepare_v2(db, selectsql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return -1;
	}

	sqlite3_bind_int64(stmt, 1, eid);

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		//this->bbsname = bbsname;
		this->id = sqlite3_column_int(stmt, 0);
		this->name = std::string((const char*)sqlite3_column_text(stmt, 2));
		this->troops = sqlite3_column_int(stmt, 3);
		this->generals = sqlite3_column_int(stmt, 4);
		this->fighters = sqlite3_column_int(stmt, 5);
		this->defence_stations = sqlite3_column_int(stmt, 6);
		this->spies = sqlite3_column_int(stmt, 7);
		this->population = sqlite3_column_int(stmt, 8);
		this->food = sqlite3_column_int(stmt, 9);
		this->credits = sqlite3_column_int64(stmt, 10);
		this->bank_balance = sqlite3_column_int64(stmt, 11);
		this->sprockets = sqlite3_column_int(stmt, 12);
		this->ore = sqlite3_column_int(stmt, 13);
		this->tech_points = sqlite3_column_int(stmt, 14);
		this->planets_food = sqlite3_column_int(stmt, 15);
		this->planets_ore = sqlite3_column_int(stmt, 16);
		this->planets_industrial = sqlite3_column_int(stmt, 17);
		this->planets_military = sqlite3_column_int(stmt, 18);
		this->planets_urban = sqlite3_column_int(stmt, 19);
		this->planets_tech = sqlite3_column_int(stmt, 20);
		this->command_ship = sqlite3_column_int(stmt, 21);
		this->last_played = sqlite3_column_int64(stmt, 22);
		this->turns_left = sqlite3_column_int(stmt, 23);
		this->total_turns = sqlite3_column_int(stmt, 24);
		this->researching = std::string((const char *)sqlite3_column_text(stmt, 25));

		sqlite3_finalize(stmt);
		sqlite3_close(db);

		return 1;
	}
	else {
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		return 0;
	}
}

int Empire::load(std::string bbsname) {
	sqlite3* db;
	sqlite3_stmt* stmt;
	static const char* selectsql = "SELECT id, bbsname, empname, troops, generals, fighters, defencestations, spies, population, food, credits, bankbalance, sprockets, ore, tech_points, planets_food, planets_ore,"
		"planets_industrial, planets_military, planets_urban, planets_tech, commandship, lastplayed, turnsleft, totalturns, researching FROM empires where bbsname = ?";
	if (!open_database(&db)) {
		return -1;
	}

	if (sqlite3_prepare_v2(db, selectsql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return -1;
	}

	sqlite3_bind_text(stmt, 1, bbsname.c_str(), -1, NULL);

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		this->bbsname = bbsname;
		this->id = sqlite3_column_int(stmt, 0);
		this->name = std::string((const char*)sqlite3_column_text(stmt, 2));
		this->troops = sqlite3_column_int(stmt, 3);
		this->generals = sqlite3_column_int(stmt, 4);
		this->fighters = sqlite3_column_int(stmt, 5);
		this->defence_stations = sqlite3_column_int(stmt, 6);
		this->spies = sqlite3_column_int(stmt, 7);
		this->population = sqlite3_column_int(stmt, 8);
		this->food = sqlite3_column_int(stmt, 9);
		this->credits = sqlite3_column_int64(stmt, 10);
		this->bank_balance = sqlite3_column_int64(stmt, 11);
		this->sprockets = sqlite3_column_int(stmt, 12);
		this->ore = sqlite3_column_int(stmt, 13);
		this->tech_points = sqlite3_column_int(stmt, 14);
		this->planets_food = sqlite3_column_int(stmt, 15);
		this->planets_ore = sqlite3_column_int(stmt, 16);
		this->planets_industrial = sqlite3_column_int(stmt, 17);
		this->planets_military = sqlite3_column_int(stmt, 18);
		this->planets_urban = sqlite3_column_int(stmt, 19);
		this->planets_tech = sqlite3_column_int(stmt, 20);
		this->command_ship = sqlite3_column_int(stmt, 21);
		this->last_played = sqlite3_column_int64(stmt, 22);
		this->turns_left = sqlite3_column_int(stmt, 23);
		this->total_turns = sqlite3_column_int(stmt, 24);
		this->researching = std::string((const char *)sqlite3_column_text(stmt, 25));

		sqlite3_finalize(stmt);
		sqlite3_close(db);

		return 1;
	}
	else {
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		return 0;
	}
}

void Empire::game_won() {
	sqlite3* db;
	sqlite3_stmt* stmt;
	static const char* sql = "SELECT id FROM empires WHERE id != ?";
	static const char* winsql = "UPDATE empires SET winner = 1 WHERE id = ?";
	if (!open_database(&db)) {
		return;
	}

	game_has_been_won = true;

	if (sqlite3_prepare_v2(db, winsql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}
	sqlite3_bind_int64(stmt, 1, id);
	sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}
	sqlite3_bind_int64(stmt, 1, id);

	while (sqlite3_step(stmt) == SQLITE_ROW) {
		send_message(sqlite3_column_int(stmt, 0), "SYSTEM", name + " has defeted the overlord and won the game!");
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);
	
}
