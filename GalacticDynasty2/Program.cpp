#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <fstream>
#include "sqlite3.h"
#include "inipp.h"
#include "Program.h"
extern "C" {
#include "magidoor/MagiDoor.h"
}
#include "Empire.h"

uint32_t turns_per_day = DEFAULT_TURNS_PER_DAY;
uint32_t turns_in_protection = DEFAULT_TURNS_IN_PROTECTION;
bool interbbs_mode = false;
std::string galaxy_name;

struct score_t {
	std::string empname;
	uint64_t score;
};

bool sort_score(struct score_t a, struct score_t b) {
	return (a.score > b.score);
}

void Program::show_scores() {
	// show the scores...
	sqlite3 *db;
	sqlite3_stmt* stmt;
	const char* sql = "SELECT bbsname FROM empires";
	std::vector<struct score_t> scores;

	if (Empire::open_database(&db) == false) {
		return;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return;
	}
	while (sqlite3_step(stmt) == SQLITE_ROW) {
		std::string bbsname = std::string((const char *)sqlite3_column_text(stmt, 0));
		Empire emp;
		if (emp.load(bbsname) == 1) {
			struct score_t s;
			s.empname = emp.name;
			s.score = emp.calculate_score();
			scores.push_back(s);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	std::sort(scores.begin(), scores.end(), sort_score);

	md_clr_scr();
	md_sendfile("ansi/score_hdr.ans", 0);

	for (struct score_t s : scores) {
		md_printf(" `bright green`%-32.32s `bright white`%" PRIu64 "\r\n", s.empname.c_str(), s.score);
	}

	md_printf("\r\n`bright white`Press any key....");
	md_getc();
}

int Program::run() {

	inipp::Ini<char> ini;
	std::ifstream is("galactic.ini");

	if (is.is_open()) {
		ini.parse(is);

		inipp::get_value(ini.sections["main"], "TurnsPerDay", turns_per_day);
		inipp::get_value(ini.sections["main"], "TurnsInProtection", turns_in_protection);
		inipp::get_value(ini.sections["main"], "InterBBSMode", interbbs_mode);
		inipp::get_value(ini.sections["main"], "GalaxyName", galaxy_name);
		is.close();
	}
	
	std::stringstream bbsname;
	std::string winner;
	bool game_has_been_won = false;
	bbsname << mdcontrol.user_alias << "!" << mdcontrol.user_firstname << " " << mdcontrol.user_lastname;
	md_clr_scr();
	md_sendfile("ansi/logo.ans", 0);
	md_getc();

	sqlite3* db;
	sqlite3_stmt *stmt;
	static const char* sql = "SELECT empname FROM empires WHERE winner = 1";

	if (!Empire::open_database(&db)) {
		return 0;
	}
	if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return 0;
	}

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		game_has_been_won = true;
		winner = std::string((const char*)sqlite3_column_text(stmt, 0));
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	Empire emp;

	emp.game_has_been_won = game_has_been_won;

	int load_result = emp.load(bbsname.str());
	bool done = false;
	while (!done) {
		md_clr_scr();
		md_sendfile("ansi/mainmenu.ans", 0);

		md_set_cursor(19, 36);
		if (load_result != 1) {
			md_printf("`bright brown`You've not played this game yet!");
		}
		else {
			struct tm lastplayed;
			struct tm today;
			time_t now = time(NULL);
#ifdef _MSC_VER
			localtime_s(&today, &now);
			localtime_s(&lastplayed, &emp.last_played);
#else
			localtime_r(&now, &today);
			localtime_r(&emp.last_played, &lastplayed);
#endif
			if (today.tm_yday != lastplayed.tm_yday || today.tm_year != lastplayed.tm_year) {
				// it's a new day
				int days_passed = 0;
				if (today.tm_year != lastplayed.tm_year)
				{
					days_passed = 365 - lastplayed.tm_yday + today.tm_yday;
				}
				else
				{
					days_passed = today.tm_yday - lastplayed.tm_yday;
				}

				if (emp.bank_balance != 0) {
					if (emp.bank_balance > 0)
					{
						std::stringstream ss;
						ss << "You've earned " << (int64_t)((long double)emp.bank_balance * 0.001f * (long double)days_passed) << " credits in interest on your bank balance!";
						emp.send_message(emp.id, "SYSTEM", ss.str());
						emp.bank_balance += (int64_t)((long double)emp.bank_balance * 0.001f * (long double)days_passed);
					}
					if (emp.bank_balance < 0)
					{
						std::stringstream ss;
						ss << "You've been charged " << (int64_t)((long double)emp.bank_balance * 0.05f * (long double)days_passed) << " credits in interest on your bank balance!";
						emp.send_message(emp.id, "SYSTEM", ss.str());
						emp.bank_balance += (int64_t)((long double)emp.bank_balance * 0.05f * (long double)days_passed);
					}
				}

				emp.last_played = now;
				emp.turns_left = turns_per_day;
				emp.save();
			}

			md_printf("`bright cyan`  Your Empire: `bright green`%s", emp.name.c_str());
			md_set_cursor(20, 36);
			md_printf("`bright cyan`Current Score: `bright white`%" PRIu64, emp.calculate_score());
			md_set_cursor(21, 36);
			md_printf("`bright cyan`  Turns Today: `bright white`%d", emp.turns_left);
			int msgs = emp.new_messages();
			if (msgs > 0) {
				md_set_cursor(14, 62);
				md_printf("`bright yellow`NEW!");
			}
		}
		if (game_has_been_won) {
			md_set_cursor(23, 36);
			md_printf("`bright yellow`This game has been won by `bright white`%s`bright yellow`!`white`", winner.c_str());
		}
		md_set_cursor(23, 79);
		char c = md_getc();
		switch (tolower(c)) {
		case '1':
			if (load_result == 0) {
				
				if (emp.new_player(bbsname.str())) {
					emp.play_game();
					load_result = 1;
				}
			}
			else if (load_result == 1) {
				if (emp.turns_left > 0) {
					emp.play_game();
				}
			}
			break;
		case '2':
			show_scores();
			break;
		case '3':
			if (load_result == 1) {
				emp.show_messages();
			}
			break;
		case 'q':
			done = true;
			break;
		}
	}

	return 0;
}