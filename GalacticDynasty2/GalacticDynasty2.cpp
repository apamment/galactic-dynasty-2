// GalacticDynasty2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>
#include <fstream>

#if !defined(_MSC_VER)
#include <unistd.h>
#endif

#include "Program.h"
extern "C" {
#include "magidoor/MagiDoor.h"
}

void inuse_rm() {
	unlink("inuse.flg");
}

int main(int argc, char **argv)
{
	Program p;

	if (argc < 2) {
		std::cout << "USAGE GalacticDynasty2 /path/to/dropfile [SOCKET]" << std::endl;
		std::cout << "  -  Supports door.sys and door32.sys" << std::endl;
		return 0;
	}


	md_init(argv[1], (argc < 3 ? -1 : atoi(argv[2])));

	std::filesystem::path inuse("inuse.flg");
	if (std::filesystem::exists(inuse)) {
		md_printf("\r\n\r\n`bright red`Sorry, this game is currently in use.`white`\r\n\r\n");
		md_exit(-1);
	}
	atexit(inuse_rm);
	std::ofstream ofs(inuse);
	ofs << "In use By: " << mdcontrol.user_alias << std::endl;
	ofs.close();

	md_exit(p.run());
	// should not be reached.
	return -1;
}
