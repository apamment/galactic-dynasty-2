#pragma once
#include <string>
#include <cinttypes>
#include "sqlite3.h"

#define DEFAULT_TURNS_PER_DAY 30
#define DEFAULT_TURNS_IN_PROTECTION 90

class Empire
{
public:
	int load(int64_t eid);
	int load(std::string bbsname);
	void save();
	void play_game();
	bool new_player(std::string bbsname);
	void game_won();
	static bool open_database(sqlite3** db);
	static int list_empires(int64_t myid);

	int get_research_level(std::string name);
	void set_research_level(std::string name, int level);
	
	int new_messages();
	void show_messages();
	void send_message(int64_t toid, std::string from, std::string msg);
	void do_battle(int64_t mid, uint32_t* tattack, uint32_t* gattack, uint32_t* fattack);
	uint64_t calculate_score();

	bool game_has_been_won;

	int64_t id;

	std::string bbsname;
	std::string name;
	uint32_t troops;
	uint32_t generals;
	uint32_t fighters;
	uint32_t defence_stations;
	uint32_t spies;

	uint32_t population;
	uint32_t food;
	uint64_t credits;
	int64_t bank_balance;
	uint32_t sprockets;
	uint32_t ore;
	uint32_t tech_points;

	uint32_t planets_food;
	uint32_t planets_ore;
	uint32_t planets_industrial;
	uint32_t planets_military;
	uint32_t planets_urban;
	uint32_t planets_tech;

	uint32_t command_ship;

	uint32_t turns_left;
	time_t last_played;
	uint64_t last_score;
	uint32_t total_turns;
	std::string researching;
};

